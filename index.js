const rulesConfig = {
	required: validateRequired,
	text: validateText,
	email: validateEmail,
	password: validatePassword
}

let hasErrors = false;

function validateRequired(field) {
	if (!field.value) {
		hasErrors = true;
		showError(field.parentNode, 'This field is required');
	}
}

function validatePassword(field) {
	if (field.value && field.value.length < 8) {
		hasErrors = true;
		showError(field.parentNode, 'Minimum 8 symbols');
	}
}

function validateText(field) {
	if (field.value && (field.value.indexOf(`'`) > -1 || field.value.indexOf(`"`) > -1)) {
		hasErrors = true;
		showError(field.parentNode, `Symbols " and ' are not allowed`);
	}
}

function validateEmail(field) {
	let rule = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if (field.value && !rule.test(field.value)) {
		hasErrors = true;
		showError(field.parentNode, 'Email is not valid');
	}
}

function showError(container, errorMessage, errorClass) {
	container.className = errorClass || 'error';

	let msgElem = document.createElement('span');

	msgElem.className = 'error-message';
	msgElem.innerHTML = errorMessage;
	container.appendChild(msgElem);
}

function resetError(container, errorClass) {
	container.className = errorClass || '';

	if (container.lastChild && container.lastChild.className === 'error-message') {
		container.removeChild(container.lastChild);
	}
}

function validate(event) {
	let form = document.querySelector('.test-form');
	let elems = form.elements;

	for (let i = 0; i < elems.length; i++) {
		let fieldName = elems[i].name;

		if (fieldName && fieldName === 'gender') {
			resetError(document.querySelector('.gender-error'), 'gender-error');
		}

		if (fieldName && fieldName !== 'notes' && fieldName !== 'gender') {
			resetError(elems[fieldName].parentNode);

			let validationRules = elems[fieldName].dataset.validItem.split(' ');

			for (let i = 0; i < validationRules.length; i++) {
				let rule = validationRules[i];

				rulesConfig[rule] && rulesConfig[rule](elems[fieldName]);
			}
		} else if (fieldName === 'gender' && !elems.gender[0].checked && !elems.gender[1].checked) {
			hasErrors = true;

			showError(document.querySelector('.gender-error'), 'Select gender', 'gender-error');
		}
	}

	if (hasErrors) {
		event.preventDefault();
	}
}
